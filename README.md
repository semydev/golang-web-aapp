# Golang Web App

An awesome photo gallery application written in Go!

This is currently my first project being built in Golang so is very much a work in progress and full of learning.

<a href="../../boards"><img src="https://i.imgur.com/VFlZttD.png" width="700" /></a>
